'use strict';

/* Setup of the toolbar button's popup menu */
const list = document.getElementById("URLlist"),
      MAX_URL_CHARS = 60,
      URL_PART_CHARS = (MAX_URL_CHARS - 2 - (MAX_URL_CHARS%2))/2,
      shortcutKeys = ['1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 
			'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
			'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
let currentTabId,
    n_items = 0,
    selectedItem = -1;

/* Use information from bg.js (the background script) to construct the popup
 * NB this doesn't work in Private Windows. */
browser.runtime.getBackgroundPage().then(bg => {  // background's global object
	currentTabId = bg.currentTabId;  // needed for navigating it to a different URL
	let urls = bg.currentTargetUrls.list;
	
	/* Create the URL list in the popup */
	if (!urls.length) {
		list.append(newElement('div', ["link"], null, "-- No URLs available --"));
		return;
	}
	let count = 0;
	urls.forEach(url => {
		let displayUrl = url.length <= MAX_URL_CHARS ? url :
			url.slice(0, URL_PART_CHARS) + '...' + url.slice(-URL_PART_CHARS);
		let newdiv = newElement('div', ["link"], 
			{shortcut: shortcutKeys[count], "data-url": url}, '', {title: url +
			'\nClick = open • Middle-click = open in new tab • Right-click = context menu'});
		newdiv.append(newElement('div', ["itemtext"], null, displayUrl),
					  newElement('div', ["shortcut"], null, shortcutKeys[count]));
		list.append(newdiv);
		count = (count + 1) % 35;
	})
	n_items = urls.length;

	/* give bg.js a callback for the contextmenu: only we can use .targetElementId */
	bg.contextHandler = handleContextMenuChoice;
}).catch((e) => {
	console.log(e);
	list.append(newElement('div', ["link"], null, "Doesn't work in Private Windows"));
});

/* Add the CSS for theme colors */
browser.theme.getCurrent().then(th => {
	if (!th.colors) return;
	if (!th.colors.popup || !th.colors.popup_text) return;
	if (!th.colors.button_hover && !th.colors.button_background_hover) return;
	// only do non-default colors if they are ALL available
	document.styleSheets[0].insertRule(`
		:root {
			--theme-bg-color: ${th.colors.popup};
			--theme-hover-color: ${th.colors.button_hover || th.colors.button_background_hover};
			--theme-txt-color: ${th.colors.popup_text};
			--theme-shortcut-color: ${th.colors.popup_text};
		}`, 2);
});

/* Set listeners on clicks on popup-menu items and key presses */
list.addEventListener('click', handleLeftClick);
list.addEventListener('auxclick', handleMiddleClick);
list.addEventListener('contextmenu', handleOpenContextMenu);

/* Allow the user time to release the keys that opened the popup 
 * e.g. if the down arrow is released a little later than Alt+Ctrl */
setTimeout(document.addEventListener, 200, 'keyup', handleKeypress);

/* Handlers for clicks in the popup and for its context-menu commands */
function handleLeftClick(e) {
	browser.tabs.update(currentTabId, {'url': e.target.closest('.link').dataset.url});
	window.close();  // close the Uppity popup
}
function handleMiddleClick(e) {
	browser.tabs.create({'url': e.target.closest('.link').dataset.url});
	window.close();
}
function handleOpenContextMenu(e) {
	browser.menus.overrideContext({showDefaults: false});
	// shade the item on which the context menu was opened
	let index = Array.prototype.indexOf.call(list.children, e.target.closest('.link'));
	if (selectedItem === index) return;
	if (selectedItem !== -1)
		list.children[selectedItem].classList.remove("context");
	list.children[index].classList.add("context");
	selectedItem = index;
}
function openInNewWindow(url) {
	browser.windows.create({'url': url});
	window.close();
}
function copyUrl(url) {
	navigator.clipboard.writeText(url);
	window.close();
}
function bookmarkUrl(url) {
	/* bookmark the url to the "Other Bookmarks" folder */
	browser.bookmarks.create({
		title: url.split('://')[1] || url,
		url: url
	});
	window.close();
}
function changeSelection(increment) {
	if (selectedItem === -1)
		selectedItem = increment===1 ? 0 : n_items-1;
	else {
		list.children[selectedItem].classList.remove("context");
		selectedItem = (selectedItem + increment + n_items) % n_items;
	}
	list.children[selectedItem].classList.add("context");
}

/* Handler for context-menu click, called from the background script */
function handleContextMenuChoice(menudata) {
	let action = menudata.menuItemId;
	let item = {target: browser.menus.getTargetElement(menudata.targetElementId)};
		// which items's context menu it was
	switch (action) {
		case "uppity_open_here":
			handleLeftClick(item); break;
		case "uppity_open_tab":
			handleMiddleClick(item); break;
		case "uppity_open_window":
			openInNewWindow(item.target.closest('.link').dataset.url); break;
		case "uppity_copy":
			copyUrl(item.target.closest('.link').dataset.url); break;
		case "uppity_bookmark":
			bookmarkUrl(item.target.closest('.link').dataset.url); break;
	}
}

/* Handler for keypresses */
function handleKeypress(event) {
	if (!event.key) return;
	let key = event.key.toUpperCase();
	let element = list.querySelector(`.link[shortcut="${key}"]`);
	if (element)
		return handleLeftClick({target: element});
	if (key === "ESCAPE")
		return window.close();
	if (key === "ARROWDOWN")
		return changeSelection(1);
	if (key === "ARROWUP")
		return changeSelection(-1);
	if (key === "ENTER" || key === " ") {
		if (selectedItem < 0) window.close();
		else handleLeftClick({target: list.children[selectedItem]});
	}
}

/* UTILITY FUNCTION */
function newElement(tag, classes, attributes, content, otherProperties) {
	let element = document.createElement(tag);
	element.classList.add(...classes);
	if (attributes) for (let a in attributes) element.setAttribute(a, attributes[a]);
	return Object.assign(element, {textContent: content || ''}, otherProperties);
}
