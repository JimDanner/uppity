## Design choices
Some explanation of the design choices made in the extension.

### What happens when you click the toolbar button
In good old Uppity from before the modern extension system, the toolbar button had a main part and a dropdown part: the part on the left was an icon, and the part on the right was an arrow. If you clicked the icon part, you'd immediately navigate to the first URL in the list; if you clicked the dropdown arrow, the hierarchy of URLs would be shown to choose from.

This was the same with the built-in *Back* and *Forward* buttons, and many extensions used something like it. **Unfortunately, Firefox got rid of it.** A toolbar button can now have just one clickable area. It is possible, with the latest WebExtensions API, to set a dropdown ('popup') menu on the left-click and an immediate action on the middle-click, but that's about it. Practically, the developer must choose between a dropdown menu and a single action from a button click.

My preferred choice is to have the popup when you click the button:

1. Personally, I very often don't want to go to the first choice but 'higher up' in the path
    - Some sites make it impossible to go up by repeatedly going *one step* up in the hierarchy. On such a site, if you're at `domain.tld/path/home/` and the extension tries to load one level up, `domain.tld/path/`, the server redirects you to `domain.tld/path/home/`. With a button that just goes one level up, you get stuck; you need to skip a level.
    - Some other sites just don't take the 'path' idea seriously, they have a large number of tiny path-segments with little meaning, which you need to skip over.
2. It is still possible - and often faster - to use a keyboard shortcut for the single action

Other users prefer the button to do something for them immediately. I have therefore made the behavior an option: right-click the button and choose the *Toolbar button click* sub-menu to make your choice. And, of course, immediate upward navigation is always a keyboard shortcut away.

### End the URLs with a slash?
Paths can be formatted with or without a trailing slash: `https://domain.tld/home/` or `https://domain.tld/home`.

In theory those can be two different pages, but no server does that. The only functional difference is in the meaning of *relative links*: if the page HTML has

```html
<img src="logo.svg">
```

then if the URL

- is `domain.tld/home` the browser fetches the image at `domain.tld/logo.svg`
- is `domain.tld/home/` the browser fetches the image at `domain.tld/home/logo.svg`.

It isn't practical to have both versions in our list, like

1. domain.tld/dir/home/
1. domain.tld/dir/home
1. domain.tld/dir/
1. domain.tld/dir
1. domain.tld/

and it isn't very useful either: servers automatically redirect 1 to 2, or the other way around. So it is better to make a choice one way or the other. By default, I use a trailing slash, because it's more common and because Uppity conceptually traverses a hierarchy of quasi-directories. I leave the *full* path unchanged from the original URL (item 2 in the example in the [Usage](https://gitlab.com/JimDanner/uppity/-/blob/main/README.md#Usage) section). You can change the behavior: right-click the Uppity toolbar button and check or uncheck "End paths with /".

### At what stage should the port number come off?
What is the place in the sequence where the next URL is one without the port number? Two options seem reasonable:

##### Option A: leave it on while the domain changes

1. `http://subdomain.domain.tld:8080/dir/`
1. `http://subdomain.domain.tld:8080/`
1. `http://www.domain.tld:8080/`
1. `http://domain.tld:8080/`
1. `http://domain.tld/`

##### Option B: first remove it, then change the domain

1. `http://subdomain.domain.tld:8080/dir/`
1. `http://subdomain.domain.tld:8080/`
1. `http://subdomain.domain.tld/`
1. `http://www.domain.tld/`
1. `http://domain.tld/`

I think it should come off after step 2 (Option B). This is because subdomains are often on different IP addresses, and often hosted on different servers, which are configured differently etc. In other words, it is unlikely that the port number that works on `subdomain.domain.tld` will still work on `www.domain.tld`.

### What to do with restricted URL schemes
In a URL like

```
about:debugging#/runtime/this-firefox
```

it might be useful to 'go up' to `about:debugging#/runtime/` and so on. Similarly, `file://` URLs have a clear hierarchy where going up makes sense.

For security reasons, **extensions are not allowed to open those URLs**. So if we are on the page `file:///home/jim/Desktop/` looking at a directory listing, the page has a clickable upward-arrow icon to reach `file:///home/jim/`, but the Uppity extension is not allowed to take us there.

For that reason, it feels like 'over-promising' to enable Uppity on pages like this. On the other hand, the popup's context-menu commands like *Copy* and *Bookmark* might still have some use. It is unclear to me what is best.
