# Events
What events fire when various typical browsing actions happen?

The interesting events about changes to URLs which the *background script* of a Webextension can observe (as opposed to its *page scripts*, which are closer to what is happening on the `window` object inside a tab), are

1. `browser.webNavigation.onCompleted`
2. `browser.webNavigation.onHistoryStateUpdated`
3. `browser.tabs.onActivated`
4. `browser.webNavigation.onReferenceFragmentUpdated`
5. `browser.tabs.onUpdated` with filter `{properties: ['url']}`
6. `windows.onFocusChanged`

The interesting properties of many event objects are:

1. `tabId`
2. `url`
3. `frameId` (0 for the main page and different for iframes)
4. `transitionType` (what caused the transition, e.g. `link`; not for every type of event)

The `tabs.onActivated` event has `e.tabId`, `e.previousTabId` and `e.windowId`.
The `windows.onFocusChanged` event has just a number as its event, which is the windowId of the newly focused window.

The `tabs.onUpdated` is exceptional as it has 3 arguments for the event handler:

- the `tabId` (a number)
- an object with one property: the `url` navigated to
- the tab object. Advantage: we don't need to retrieve the tab object ourselves, we already have it => no more async function calls.

There are many other events but they are probably less interesting. `webNavigation.onCommitted` has the interesting `transitionType` property.

## Events within a tab

#### 1. Click on a normal link in a tab
- `webNavigation.onHistoryStateUpdated` with `tabId`, with the `url` you navigate to
- `webNavigation.onCompleted` with `tabId` and the `url` navigated to
- `tabs.onUpdated` comes *twice*. The second time, the tab object is different in terms of `.title` and `.lastAccessed`.

Also the events `webNavigation.onBeforeNavigate`, `webNavigation.onCommitted`, and `webNavigation.onDOMContentLoaded` fire somewhere along the way, all with the same `url`, `tabId` and `frameId=0`.

#### 2. Click on a link at github.com or similar, where a page script changes the content and the URL, as in a kind of 'one-page app'

- `webNavigation.onHistoryStateUpdated` comes *twice* with a `tabId`, with the `url` you navigate to; only `.lastAccessed` is different between them
- `tabs.onUpdated` comes *twice*, this time with the same (outdated) `title` and only a difference in `.lastAccessed`.

#### 3. Click on a link to a # handle in the current page
- `webNavigation.onReferenceFragmentUpdated`
    It has `transitionType='link'` and `frameId = 0`; the `url` is the new url with the # fragment.
- ` tabs.onUpdated` with the usual stuff (incl. the new URL)

#### 4. Script makes XHR requests to update some contents
This sends no event, as it shouldn't (there is no navigation going on).

#### 5. Some page contents from another domain are requested as part of the page
If this is just some image, there is *nothing*. We see events *only when the requested file is a full HTML document*, in an *iframe*.

- `webNavigation.onBeforeNavigate`, ` webNavigation.onCommitted`, `webNavigation.onDOMContentLoaded` all with `frameId != 0`
- `webNavigation.onCompleted` with `tabId` and the `url` loaded

and for some of these requests, sometimes, additionally

- `webNavigation.onHistoryStateUpdated` with a `tabId`, with the `url` requested, with `frameId != 0`, twice

Sometimes the *final* event is `webNavigation.onCompleted` for the main `url` of the page, but in other cases some iframes emit their event *later* than the main page.

Importantly, the `browser.tabs.onUpdated` event does **not** fire.

#### 6. Iframe contents from another domain are requested by a script during page build-up
Same as *5. Some page contents from another domain are requested as part of the page*

#### 7. Write a URL in the URL bar, which opens in the current tab
- `webNavigation.onBeforeNavigate`, `webNavigation.onCommitted`, `webNavigation.onDOMContentLoaded`, `webNavigation.onCompleted` all fire once, with the right parameters.
- `tabs.onUpdated` fires once or twice, with the same parameters, and the second time has an updated title.

If the HTTP response redirects the request to a different domain (e.g. cnn.com is redirected to edition.cnn.com), the `webNavigation.onBeforeNavigate` event still has the initial `url`, but everything after that - including the (only) `tabs.onUpdated` - already uses the redirected domain.

#### 8. Open a bookmark in the current tab
- `webNavigation.onBeforeNavigate`, `webNavigation.onCommitted`, `webNavigation.onDOMContentLoaded`, `webNavigation.onCompleted` all fire once, with the right parameters. No `webNavigation.onHistoryStateUpdated`. 
- `tabs.onUpdated` fires once or twice, with the same parameters, and the second time has an updated title.

#### 9. Open a history item in the current tab
Same as *8. Open a bookmark in the current tab*

#### 10. Open a new search in the current tab
- `webNavigation.onBeforeNavigate`, `webNavigation.onCommitted`, `webNavigation.onHistoryStateUpdated`, `webNavigation.onDOMContentLoaded`, `webNavigation.onCompleted` all fire once, with the right parameters.
- `tabs.onUpdated` fires twice, with the same parameters, and the second time has an updated title.

#### 11. Reload the tab
Same as *8. Open a bookmark in the current tab*

#### 12. Automatic reload (by a Greasemonkey script, for example)
Same as *8. Open a bookmark in the current tab*, except there's no `webNavigation.onBeforeNavigate` event.

#### 13. Go back in the history in the current tab
Even when the page is taken entirely from cache,

- `webNavigation.onBeforeNavigate`, `webNavigation.onCommitted`, `webNavigation.onDOMContentLoaded` (sometimes), `webNavigation.onCompleted` all fire once, with the right parameters.
- If it is only a change to/from a *# tag* within the page, instead of the 3 above, a `webNavigation.onReferenceFragmentUpdated` event fires.
- `tabs.onUpdated` fires once, with the same parameters.

#### 14. Go forward in the history in the current tab (after having gone backward)
Same as *13. Go back in the history in the current tab*.

## Tabs

#### 15. Click to open a link in a new tab
- `tabs.onActivated` fires with the new `tabId` but with no `url`-like thing, where the tab retrieved with the returned id still has `status: "loading"`.
- `webNavigation.onBeforeNavigate`, `webNavigation.onCommitted`, `webNavigation.onDOMContentLoaded`, `webNavigation.onCompleted` all fire once, with the right parameters 
- `tabs.onUpdated` fires once or twice, with the right data (though in opening the extension debugger, the first one has `about:blank` as its `url`).

#### 16. Switch tab normally
Only `tabs.onActivated` fires, nothing else; the event object has `tabId` of the tab navigated *to*, and `previousTabID` of the one navigated *from*. When the tab is then retrieved, it has `status = "complete"`.

***Question: does it reliably have that status?***
It seems to.

#### 17. Switch to an inactive tab after startup with session-restore
- `tabs.onActivated` fires with the new `tabId` but with no `url`-like thing. The tab retrieved after this has `status = "loading"`
- all the navigation events fire
- `tabs.onUpdated` fires 3 times: first with the correct url (which was probably in the cache), then within 20 ms with `about:blank`, and then after 75 ms with the correct url again.

If we retrieve the tab DOM object after the first `tabs.onActivated`, which is what you'd do in case of a *normal* tab switch (between fully active, loaded tabs), even though it takes about 42 ms, the result still has the wrong contents: `about:blank`.

On the positive side, the final `tabs.onUpdated` event will put everything right; but we are adding work to do for the browser at its busiest time and it may make the toolbar button go on and off.

If tab switches reliably give tabs with `status = "complete"`, we can simply ignore the data when the status is different, so the later events can do their work. Still, we will have 3 events that change everything back and forth.

We could save the tab's `lastAccessed` time for every `tabs.onUpdated` event, and then when we get one, see if it comes very shortly after another one and ignore it if it does. This will make only the first one effective. Or we ignore it if it's short after another one *and* has `url: "about:blank"`. Such solutions are a lot more efficient than setting timers to do debouncing.

#### 18. Open a new tab by means of a script or extension (uppity+ or other)
Does the same as *15. Click to open a link in a new tab*.

#### 19. Duplicate a tab
Does the same as *17. Switch to an inactive tab after startup with session-restore*.

#### 20. Write a URL in the URL bar, which opens a new tab
Does the same as *15. Click to open a link in a new tab*, where the `tabs.onActivated` still has `status: "loading"`.

#### 21. Open a bookmark in a new tab
Does the same as *15. Click to open a link in a new tab*, where the `tabs.onActivated` still has `status: "loading"`.

#### 22. Open a history item in a new tab
Does the same as *15. Click to open a link in a new tab*, where the `tabs.onActivated` still has `status: "loading"`.

#### 23. Open a new tab from the search bar
Does the same as *15. Click to open a link in a new tab*, where the `tabs.onActivated` still has `status: "loading"`.

#### 24. Change the tab by closing the current one
Only `tabs.onActivated` fires, nothing else; the event object has `tabId` of the tab navigated *to*, and `previousTabID` which is undefined. When the tab is then retrieved, it has `status = "complete"`.

## Windows

#### 25. Click to open something in a new window
- `tabs.onActivated` fires with the new `tabId` *and* the new `windowId`. If you ask for that tab, you get something with title "New tab", url `about:blank`, but still `status: "complete"`.
- `windows.onFocusChanged` fires twice: with windowId -1 and then with the new id of the newly created window, which upon query contains that same weird tab object.
- `webNavigation.onCreatedNavigationTarget` (not seen before), `webNavigation.onBeforeNavigate`, `webNavigation.onCommitted`, `webNavigation.onDOMContentLoaded`, `webNavigation.onCompleted` all fire once, with the right parameters.
- `tabs.onUpdated` fires once or twice, with the right data.

So it gets even more complicated: we now have a `tabs.onActivated` that should not do stuff, but the tab it opens claims to be completed - unlike scenario 17. How are we to know this should not update the button?

We could say: if `tabs.onActivated` fires with a `windowId` that's not the current one, it *must save the new ID*. Thus, after being -1, the windowId becomes the old one again and the `windows.onFocusChanged` is ignored. *Rule*: ignore `windows.onFocusChanged` if the window-id is the current one (saved in a variable).

Reading a new active tab should also set our global variable for windowId, and when we get a window-change event we check whether that is still where we were. 
The order of arrival is:

- `tabs.onActivated` (new tabId)
- `windows.onFocusChanged` (-1, then the new windowId)
- information about the tab, asked by the first handler
- the list of (one) active tab, identical to the former.

So by returning as the window id isn't right, we are not losing any information.
One thing to consider: having the button temporarily at `about:blank` is still better than having it at the previous active tab's URL: that makes no sense at all.

#### 26. Open in new window by means of a script or extension (uppity+ or other)
Does the same as *25. Click to open something in a new window*.

#### 27. Change to another active window
This only triggers
	windows.onFocusChanged
which calls the handler with one argument, the `windowId` of the newly focused window (or -1 if it's not a window of the browser).

After that, if it's not -1 and not the window that most recently had focus before there was a switch to a different app and back, you must find out the active tab with `browser.tabs.query({'active': true, 'currentWindow': true})` or instead of that last one you could use `windowId: event`. 

***Question: what happens when it's not a browsing window, e.g. the bookmarks window?***
Those windows also trigger this event. But the window object with that id is an error. If you get the current tabs (with currentWindow: true) you get the tabs from the 'normal' browser window, not the Library window (for bookmarks downloads and history). So that might go wrong. But if you get the tabs with `windowId: event` you get an empty array, for which you might gracefully end the process.

Opening the web developer tools triggers no event.

**Remarkable**
The query tabs.query seems to take relatively little time, sometimes 25ms but sometimes just 2.

#### 28. Change to a different tab than the active one in a different window
This triggers two events shortly after one another:

- `windows.onFocusChanged`
- `tabs.onActivated`

The array of tabs retrieved after the window change arrives after the second event notification, and *already contains the tab to which we have switched*.
So either we simply retrieve the new tab twice, with no adverse consequences except wasted resources, or we create some sort of handling for a quick sequence of two events.
A thing to remember in all of the *Window* events is that opening a new window takes a lot of time anyway, so the user won't be bothered. On the other hand: slowing down something that's already a bit slow is something you'd rather not do.

#### 29. Change to a sleeping tab in a different window
- `windows.onFocusChanged`, and when queried it gives the newly selected tab, with `about:blank`
- `tabs.onActivated`, which can then retrieve the tab which has `status: "loading"` and url `about:blank` (so it will be ignored due to the status)
- all the navigation events fire
- `tabs.onUpdated` fires 3 times: first with the correct url (which was probably in the cache), then within 20 ms with `about:blank`, and then after 75 ms with the correct url again, so in all:
	1. correct URL, `status: "complete"`
	2. `about:blank`, `status: "complete"`
	3. correct URL, `status: "loading"`

We haven't used `status` for the `tabs.onUpdated` event so far (only for `tabs.onActivated`), and it is clear we shouldn't.

The reponse to the first query comes in around nr 2, so it makes sense that it has `about:blank`. This can be mitigated by checking that returned tab for its `lastAccessed` property and ignoring this if it's too brief after nr 1. 

Conclusion: we should ignore `tabs.onUpdated` that

- have `about:blank`, and
- come shortly after another one.

Thus, we must save the timestamp.

#### 30. In an unfocused window, a script opens a new tab
Probably `tabs.onActivated`, but with a `windowID` that isn't the currently active one, so we can easily avoid it triggering any action in the background script.