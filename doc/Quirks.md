## Edge cases and weird rules
Uppity+ uses the browser's built-in URL parser. Using an object of type [Location](https://developer.mozilla.org/en-US/docs/Web/API/Location), we can read off and edit the individual parts of a URL. For example, after we set `location.href` to the URL

`https://jim:gH5x9@sub.example.com:21/path/file.html?lan=en#Sec3`

the browser recognizes these parts:

Property | Text
---      | ---
`location.protocol` | https:
`location.username` | jim
`location.password` | gH5x9
`location.hostname` | sub.example.com
`location.port` | 21
`location.pathname` | /path/file.html
`location.search` | ?lan=en
`location.hash` | #Sec3

But some unexpected cases can occur.

### Hashes in unusual places
A normal fragment indicator is a hash followed by further characters, at the very end of a URL, like `#Sec3` in the example.

The [standard](https://gitlab.com/JimDanner/uppity/-/blob/main/doc/URL-definition.md) says only one `#` can be in the URL, and *the string after it* is formally called the 'fragment'. Usually, therefore, `location.hash` consists of `#` + the fragment. 

#### Hash at the end of the URL
There is one exception: the [documentation](https://developer.mozilla.org/en-US/docs/Web/API/Location/hash) says,

> If the URL does not have a fragment identifier, this property contains an empty string, "".

This implies that **a URL ending in a hash** (allowed by the standard and used in places like `docs.google.com/document/d/***/edit#`), and thus with an empty fragment, has an empty `location.hash`. Just in that one case, the `#` does not become part of the `hash` property.

So in that case, the extension must remove `#` not by setting `.hash=''` but by other means, at the point where normally the fragment is removed.

#### Hash followed by a 'path' with slashes
Various characters are allowed to follow the `#`, *even the slash character*. So if you type a URL like

<https://econfigure.xe.abb.com/global/#/categories/9AAC189572>

into the URL bar, *everything from the `#`* is the 'fragment'. The browser will fetch the page `https://econfigure.xe.abb.com/global/` and will search that page to find an element whose `name` or `id` is '/categories/9AAC189572'. What happens next is probably heavily scripted, but the point is that we have something that looks like a 'path' but isn't.

In principle, as we climb the URL hierarchy, the fragment comes off in one go to yield `https://econfigure.xe.abb.com/global/`. But I assume that users will want to traverse the fake 'path' inside the fragment, visiting `https://econfigure.xe.abb.com/global/#/categories` first.

This particular website indeed shows a meaningful page when you change the fragment that way (obviously it doesn't send another request to the server because it's 'staying on the same page' &ndash; which is probably the reason for this site's setup). So we go 'up' within the 'path' that is part of the fragment, before we remove `location.hash`.

### Question mark at the end
A situation similar but different to the hash-at-the-end exists for the query string, which starts with `?`. There can be **a URL ending in a question mark**: `location.search` is "", and so on. But now, if you set `.search=''`, it actually changes `.href`: the final `?` is removed. So 

`location.search = location.search`

actually removes the question mark.

### Other delimiters without data
For other delimiters known to the URL standard, there seem to be no quirks:

* an **empty userinfo part** is immediately removed: after entering `location.href='http://@sub.dom.com/doc'` the value is actually set to `http://sub.dom.com/doc`, without the lonely `@`
* an **empty port** is immediately removed: after entering `location.href='http://sub.dom.com:/doc'` the value is actually set to `http://sub.dom.com/doc`, without the lonely `:`

As the table above shows, the delimiters `@` and `:` don't become part of the `location` properties, which seems to keep things more orderly.

### Double slash
It is perfectly legal to put two slashes after each other in a path. It means the name of one path-segment is the empty string. So `http://domain.tld/path/` is different from `http://domain.tld/path//` and might have a different function on a website (please let me know if you see actual examples of double slashes in paths).

Perhaps taking the slashes off one by one is a little tedious, but since I can't be sure no-one actually distinguishes pages this way, I'll leave this in. Thus, `http://domain.tld.path//file.htm` will give this list in the popup:

1. `http://domain.tld/path//`
1. `http://domain.tld/path/`

and so on.