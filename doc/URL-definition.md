# Analyzing URLs

## URI/URL definition (RFC 3986)
Definition of the URI; URLs also follow this definition.
Source: [IETF](https://datatracker.ietf.org/doc/html/rfc3986#appendix-A)

The syntax used in this definition is a modified Backus-Naur Form:

* uses `=` instead of `:=`
* comments after `;`
* literals between double quotes
* symbols, with their Regex equivalent:
    - `/`means `|` (*or*)
    - `iX` means `X{i}` (*a sequence of i times the atom X*)
    - `i*jX` means `X{i,j}` (*at least i, at most j X'es*)
    - `*X` means `X*` (*0 or more*)
    - `1*X` means `X+` (*1 or more*)
    - `[X]` or `*1X` means `X?` (*0 or 1*)
* character classes and special characters:
    - `ALPHA` (letters)
    - `CR` (carriage return)
    - `DIGIT` (decimal digits)
    - `DQUOTE` (double quote)
    - `HEXDIG` (hexadecimal digits)
    - `LF` (line feed)
    - `SP` (space)

### URI
The default form of a URI, such as a full URL, follows this definition.

```
   URI           = scheme ":" hier-part [ "?" query ] [ "#" fragment ]

   scheme        = ALPHA *( ALPHA / DIGIT / "+" / "-" / "." )

   hier-part     = "//" authority path-abempty
                 / path-absolute
                 / path-rootless
                 / path-empty

   authority     = [ userinfo "@" ] host [ ":" port ]
   userinfo      = *( unreserved / pct-encoded / sub-delims / ":" )
   host          = IP-literal / IPv4address / reg-name
   port          = *DIGIT

   IP-literal    = "[" ( IPv6address / IPvFuture  ) "]"

   IPvFuture     = "v" 1*HEXDIG "." 1*( unreserved / sub-delims / ":" )

   IPv6address   =                            6( h16 ":" ) ls32
                 /                       "::" 5( h16 ":" ) ls32
                 / [               h16 ] "::" 4( h16 ":" ) ls32
                 / [ *1( h16 ":" ) h16 ] "::" 3( h16 ":" ) ls32
                 / [ *2( h16 ":" ) h16 ] "::" 2( h16 ":" ) ls32
                 / [ *3( h16 ":" ) h16 ] "::"    h16 ":"   ls32
                 / [ *4( h16 ":" ) h16 ] "::"              ls32
                 / [ *5( h16 ":" ) h16 ] "::"              h16
                 / [ *6( h16 ":" ) h16 ] "::"

   h16           = 1*4HEXDIG
   ls32          = ( h16 ":" h16 ) / IPv4address
   IPv4address   = dec-octet "." dec-octet "." dec-octet "." dec-octet

   dec-octet     = DIGIT                 ; 0-9
                 / %x31-39 DIGIT         ; 10-99
                 / "1" 2DIGIT            ; 100-199
                 / "2" %x30-34 DIGIT     ; 200-249
                 / "25" %x30-35          ; 250-255

   reg-name      = *( unreserved / pct-encoded / sub-delims )

   path          = path-abempty    ; begins with "/" or is empty
                 / path-absolute   ; begins with "/" but not "//"
                 / path-noscheme   ; begins with a non-colon segment
                 / path-rootless   ; begins with a segment
                 / path-empty      ; zero characters

   path-abempty  = *( "/" segment )
   path-absolute = "/" [ segment-nz *( "/" segment ) ]
   path-noscheme = segment-nz-nc *( "/" segment )
   path-rootless = segment-nz *( "/" segment )
   path-empty    = 0<pchar>

   segment       = *pchar
   segment-nz    = 1*pchar
   segment-nz-nc = 1*( unreserved / pct-encoded / sub-delims / "@" )
                 ; non-zero-length segment without any colon ":"

   pchar         = unreserved / pct-encoded / sub-delims / ":" / "@"

   query         = *( pchar / "/" / "?" )

   fragment      = *( pchar / "/" / "?" )

   pct-encoded   = "%" HEXDIG HEXDIG

   unreserved    = ALPHA / DIGIT / "-" / "." / "_" / "~"
   reserved      = gen-delims / sub-delims
   gen-delims    = ":" / "/" / "?" / "#" / "[" / "]" / "@"
   sub-delims    = "!" / "$" / "&" / "'" / "(" / ")"
                 / "*" / "+" / "," / ";" / "="
```

The rule for `IP-literal` was expanded by [RFC 6874](https://datatracker.ietf.org/doc/html/rfc6874) to become

```
   IP-literal = "[" ( IPv6address / IPv6addrz / IPvFuture  ) "]"

   IPv6addrz = IPv6address "%25" ZoneID

   ZoneID = 1*( unreserved / pct-encoded )
```

#### Special case: `file:`
According to the definition above, the first two slashes denote the start of the `authority` part. This is where the server is specified, from which the resource is to be fetched. In other words, `//` begins a journey over the network.

A file on the local machine doesn't require that journey, so it makes more sense that the `hier-part` of a file URI consists of a `path-absolute` only, as in

```
file:/home/jim/Desktop/URL-definition.md
```

But ususally, `file:` is followed by three slashes. This is awkward in light of the URI standard (above). The file URL has an empty `authority` part but still puts in the slashes that start it. It's allowed by the syntax but seems redundant.

It is also philosophically wrong: `/` means 'start at the root of this machine', and `//` means 'go further: start at the root of the Internet'. A file URL only does the former, not the latter, but the three slashes suggest it goes furthest of all and starts at the root of the universe.

Standards and browser builders have made pronouncements on this:

* The original 1994 URL standard, [RFC 1738](https://www.ietf.org/rfc/rfc1738.txt) by Tim Berners-Lee *et al.*, states in section 3.10:

    > A file URL takes the form:
    >
    >     file://<host>/<path>
    >
    > As a special case, \<host\> can be the string "localhost" or the empty
   string; this is interpreted as 'the machine from which the URL is
   being interpreted'.
   
   So in the case of the local host, the host can be omitted and we get the customary three slashes, sitting uneasily with the definitions above.
* The 2017 standard [RFC 8089](https://datatracker.ietf.org/doc/html/rfc8089), *The "file" URI Scheme*, deviates from both previous standards. It makes explicit that either 1 or 3 slashes are allowed &ndash; not 2, which the modern standard above would accept (if both authority and path-abempty are empty), and not only 3 as in the original standard. The syntax comes down to

        file-URI  =  "file:" [ "//" ["localhost" / host] ] path-absolute
    
    with `host` and `path-absolute` as above. The 2017 standard says in Appendix A:

    > The syntax given (...) makes the entire authority component, including the double slashes "//", optional.
* Firefox and Safari seem to require the 3-slash version: if you input a one-slash file URI in the address bar, the browsers add two slashes after `file:`.

### URI-reference
A reference from within some URI location to another one, or a *relative* URL.

```
   URI-reference = URI / relative-ref

   relative-ref  = relative-part [ "?" query ] [ "#" fragment ]

   relative-part = "//" authority path-abempty
                 / path-absolute
                 / path-noscheme
                 / path-empty
```

### Absolute URI
In some cases, fragments are not allowed, for instance if you define your own scheme `skype://`.

```
   absolute-URI  = scheme ":" hier-part [ "?" query ]
```

## URI, URL and the rest
The term **URL** has been used as a synonym for **URI** amid a huge amount of confusion about the possible distinctions between these and other terms. The authority on the web, the W3 consortium, uses the terms interchangably, saying things like ["URIs, aka URLs"](https://www.w3.org/Addressing/#background).

The [original distinction](https://webmasters.stackexchange.com/a/77783) was:

* **URI** (identifier) is the general term: it denotes all resource-identifiers that follow this syntax, which includes the subtypes:
* **URL**s (locators) tell you "how to locate the resource". For example,
    - `http:` tells you to establish a TCP session with the location after `//` and send an *HTTP GET* request for the path starting with `/` in order to retrieve the resource;
    - `https:` says: establish the TCP session, within that establish a *TLS session* and within that send the *HTTP GET* request;
    - `ftp:` advises the establishment of a TCP session and then an *FTP connection* followed by a *RETR* request to retrieve the resource;
    - `git:` says: let the `git` program establish a TCP connection and retrieve the resource using its protocol;
    - `tel:1-888-555-5555` asks you to pick up your phone.

    The use of these schemes, and others like `mailto:`, `wss:` and `file:`,  often means we have a URL. Relative URLs like `/logo.svg` are URLs if they are resolved against something that already is a URL, like the current location of the file where the relative link is from, e.g. `https://example.com/`.
* **URN**s (names) uniquely *name* a resource without telling you how to retrieve it. They can have a form like `urn:isbn:0451450523` (a specific book; you'll need to figure out yourself how to get to it). To add some confusion, URNs may look like locators but really have the *function* of a name, such as this URN denoting a certain file syntax: `http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul`. The user is not supposed to retrieve this resource from the location; it is assumed the user already knows about XUL, which is only named uniquely with this URN.
* **URC**s (citations) are a sort of meta-resources: they tell you something *about* a resource. `view-source:...` shows the source code, for example of a web page. In Firefox, `about:reader?url=...` shows a web page in Reader View.
* **Data URI**s don't give information about any resource at all, they *are* the resource. `data:image/png;base64,iVBORw0...` contains an image.

For me, this is not a completely exhaustive definition of the differences. For example, is <about:support> (Firefox's *Troubleshooting Information* page) a URL? It sort-of 'locates' that page, but it doesn't contain instructions for how or where to retrieve it. The page gets displayed (so, apparently, Firefox managed to locate it) because *in the context of* Firefox it's clear where it is. The same can be said of <chrome://browser/content/aboutRobots.xhtml>. And if those two are not URLs because there are no retrieval instructions, then is `file:///boot.cfg` a URL? That can also be located by Firefox only *in the context of* the machine Firefox is running on.

## Built-in parser
Happy that we don't have to worry about vague distinctions betweer URI and URL, we let Firefox analyze whatever is in its address box (a.k.a. URL bar). We retrieve it with `window.location.href` or `tab.url` and use the built-in `URL` class as a parser.

First we create an instance of the class:

```
let a = new URL('about:config');
```

The `URL` class more or less implements the `Location` protocol, though it has its own definition - its prototype is `URLPrototype`.

### Properties
If we have a `URL` instance in the variable `location` and we set

```
location.href = https://jim:gH5x9@sub.example.com:21/path/file.html?lan=en#Sec3
```

the object has these properties:

Property | Text | Item from RFC 3986
---      | ---  | ---
`location.protocol` | https: | `scheme` + `:`
`location.username` | jim | part of `userinfo`
`location.password` | gH5x9 | part of `userinfo`
`location.hostname` | sub.example.com | `host`
`location.port` | 21 | `port`
`location.pathname` | /path/file.html | `path`
`location.search` | ?lan=en | `?` + `query`
`location.hash` | #Sec3 | `#` + `fragment`

In addition, there is a property `location.searchParams` that contains each individual part of the `query` as a key-value pair. It can be used by means of various methods, such as `keys()`, `has()`, `forEach()`, `get()`, `set()`, `delete()`, `append()`.

### Constructor
The constructor for the `URL` object has various restrictions and presets. For example, the scheme determines how it interprets the rest of the input:

* `new URL('file:hello')` creates the URL `file:///hello`
* `new URL('http:hello')` creates the URL `http://hello/` that has `host='hello'`, `pathname='/'`
* `new URL('about:config')` puts `config` in `pathname`, but if you then change `.protocol='http:'` it puts `config` in `host` and sets `pathname='/'`.

