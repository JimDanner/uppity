# Name of the file to make; files to compile
RESULT = uppityplus@jimdanner.xpi
SOURCES = .
EXCLUDE = _* .* '**/.*' '.git/*' 'doc/*' 'updates/*' README.md Makefile

# Compilation parameters
CC = zip
CFLAGS = -r
EXCLUDEFLAG = -x

# Targets
all: clean compress

compress:
	$(CC) $(CFLAGS) $(RESULT) $(SOURCES) $(EXCLUDEFLAG) $(EXCLUDE)

clean:
	rm -f $(RESULT)
