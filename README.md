# Uppity+

Update of [Uppity](https://addons.mozilla.org/en-US/firefox/addon/uppity/) for the WebExtensions API. Firefox-specific.

Install, while in Firefox, by clicking on the .xpi file, under [Releases](https://gitlab.com/JimDanner/uppity/-/releases). Put the toolbar button <img src="https://gitlab.com/JimDanner/uppity/-/raw/main/skin/icon32.png" width="20px" height="20px"> in a toolbar.

## Usage
On pages with regular URLs (not `about:`... etc.) the **toolbar button** is available. Clicking it displays the list of possible 'upward' changes. 

For example, if you are on a page with some data in the URL, in which you clicked a link to get to a different section in the same page, the URL might look like

```
https://subdomain.example.com/path/file.html?lan=en#Section3
```

and the list of URLs in the popup will be:

1. `https://subdomain.example.com/path/file.html?lan=en` (without the internal section link)
1. `https://subdomain.example.com/path/file.html` (without data in the URL)
1. `https://subdomain.example.com/path/` (we start going 'up', as an uppity extension would want to)
1. `https://subdomain.example.com/`
1. `https://www.example.com/` (a little detour on the way up the hierarchy, because `www` servers are ubiquitous)
1. `https://example.com/` (the 'root' of the hierarchy)

Very long URLs can be seen in full in the mouse-over tooltip.

<img src="https://gitlab.com/JimDanner/uppity/-/raw/main/doc/Dark mode.png" width="70%">

Each of the **address list items** in the list can be clicked:

* a normal click opens the URL in the current tab
* a middle-click opens it in a new tab
* a right-click opens a context menu with the following options:
    - *Open in this tab*
    - *Open in new tab*
    - *Open in new window*
    - *Copy this URL* (requires an optional permission: *Input data to the clipboard*. Without the permission, it is greyed out.)
    - *Share* (not working; hopefully Firefox will make its built-in share menu available for extensions)
    - *Bookmark this URL* (saves the bookmark to Other Bookmarks; this requires an optional permission: *Read and modify bookmarks*. Without the permission, it is greyed out.)

<img src="https://gitlab.com/JimDanner/uppity/-/raw/main/doc/Popup context menu.png" width="70%">

Instead of clicking the toolbar button, you can use **keyboard shortcuts**:

* go up by one level immediately (to item 1 in the example) with <kbd>Ctrl</kbd>+<kbd>Alt</kbd>+<kbd>Up</kbd> (on macOS: <kbd>Cmd</kbd>+<kbd>Alt</kbd>+<kbd>Up</kbd>)
* open the popup menu on the toolbar button with <kbd>Ctrl</kbd>+<kbd>Alt</kbd>+<kbd>Down</kbd> (on macOS: <kbd>Cmd</kbd>+<kbd>Alt</kbd>+<kbd>Down</kbd>)

In the popup menu, the up and down arrow keys select an item; <kbd>Enter</kbd> or <kbd>Space</kbd> opens it in the current tab. Alternatively, press the number or letter displayed next to the item you want to open. <kbd>Esc</kbd> closes the popup menu, as do <kbd>Enter</kbd> and <kbd>Space</kbd> as long as no item has been selected.

### Settings
Changing the **keyboard shortcuts** can be done on the *Add-ons and Themes* page of Firefox. On that page, click the cog on the top right and choose *Manage Extension Shortcuts*.

Granting and revoking the **optional permissions** is also done on the *Add-ons and Themes* page. Click the extension in the list and go to *Permissions*. [Or, faster: right-click the Uppity toolbar button and choose *Manage Extension*.]

The *Input data to the clipboard* permission enables copying of URLs from the list in the popup, using the context menu. *Read and modify bookmarks* allows you to bookmark URLs from the popup menu using the context menu.

Changing the **settings** of Uppity: right-click the toolbar button. At the top of the context menu, you can access six options:

<img src="https://gitlab.com/JimDanner/uppity/-/raw/main/doc/Settings.png" width="70%">

* *Show number of links on button* &ndash; display or hide a badge with the number of URLs in the popup (it would show <kbd>6</kbd> in the example above).
* _Include www\.* item_ &ndash; toggle the presence of the item where the domain gets `www.` at its start, item 5 in the example above.
* *End paths with /* &ndash; toggle the trailing slash at the end of paths, like in item 3 in the example above. For further information, see the [considerations](https://gitlab.com/JimDanner/uppity/-/blob/main/doc/Choices.md#end-the-urls-with-a-slash) around this.
* *Keyboard shortcuts* &ndash; enable or disable use of the extension's keyboard shortcuts.
* *Split query into parameters* &ndash; take the query string off in one go, or one parameter at a time. If this option is enabled, then from `google.com/?client=firefox&q=uppity` you'd first go to `google.com/?client=firefox` and then to `google.com/`.
* *Toolbar button click* &ndash; what the toolbar button does:
    - *List of addresses* &ndash; display a list of URLs you can switch to, as shown in the Usage guide above
    - *Go up one level* &ndash; instead of choosing from a list, you can configure that a click on the button takes you to the first web address in the list: "up one level" in the URL hierarchy. A middle-click on the button will open that address in a new tab.

<img src="https://gitlab.com/JimDanner/uppity/-/raw/main/doc/Button settings.png" width="70%">

All settings are preserved across restarts of the browser.

## Further reading
The `doc` directory has further explanations, including

* [Design choices](https://gitlab.com/JimDanner/uppity/-/blob/main/doc/Choices.md) that are inevitable in writing an extension
* [Edge cases](https://gitlab.com/JimDanner/uppity/-/blob/main/doc/Quirks.md), quirks of the various APIs, and plainly weird rules.
