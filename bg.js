'use strict';

/* SETUP: constants and global variables */
const identifier = browser.runtime.getURL(""),
      DEBOUNCE_MS = 120,
      contextMenuItems = {  /* The popup-list context menu */
		"uppity_open_here": {title: "Open in this &tab"},
		"uppity_open_tab": {title: "Open in &new tab"},
		"uppity_open_window": {title: "Open in new &window"},
		"sep1": {type: "separator"},
		"uppity_copy": {title: "&Copy this URL"},
		"uppity_share": {title: "&Share"},
		"uppity_share_please": {title: "Firefox, please implement this", 
				parentId: "uppity_share", enabled: false},
		"uppity_bookmark": {title: "&Bookmark this URL"}
      },
      settingsMenuItems = {
		"uppity_buttonbadge": {option: "badgeOnButton",
			properties: {title: "Show &number of links on button", type: "checkbox"}},
		"uppity_www_urls": {option: "addWwwToList",
			properties: {title: "Include &www.* item", type: "checkbox"}},
		"uppity_trailing_slash": {option: "finalSlash",
			properties: {title: "End &paths with /", type: "checkbox"}},
		"uppity_shortcuts": {option: "shortcutKeys",
			properties: {title: "&Keyboard shortcuts", type: "checkbox"}},
		"uppity_query_parts": {option: "urlQueryParts",
			properties: {title: "Split &query into parameters", type: "checkbox"}},
		"uppity_buttonclick": {properties: {title: "&Toolbar button click"}},
		"uppity_buttonpopup": {option: "buttonBehavior", value: "popup",
			properties: {title: "&List of addresses", type: "radio",
			parentId: "uppity_buttonclick"}},
		"uppity_buttonup": {option: "buttonBehavior", value: "up",
			properties: {title: "Go &up one level", type: "radio",
			parentId: "uppity_buttonclick"}},
		"uppity_buttonmixed1": {option: "buttonBehavior", value: "popup+upnewtab",
			properties: {title: "Left=list, middle=new tab", type: "radio",
			parentId: "uppity_buttonclick"}},
		"uppity_buttonmixed2": {option: "buttonBehavior", value: "popup+up",
			properties: {title: "Left=list, middle=up one level", type: "radio",
			parentId: "uppity_buttonclick"}}
      },
      defaultOptions = {
		badgeOnButton: false,
		addWwwToList: true,
		finalSlash: true,
		urlQueryParts: false,
		shortcutKeys: true,
		buttonBehavior: "popup",
		hideContextMenuItems: [],
		disableForSchemes: ['about:', 'chrome:', 'data:', 'file:', 'javascript:']
      };
this.contextHandler = () => {console.warn("No popop-context-menu handler")};
this.options = null;
this.currentWindowId = 0;
this.currentTabId = 0;
this.currentUrl = '';
this.currentTargetUrls = {};
this.lastUpdateTimestamp = 0;
browser.browserAction.setBadgeBackgroundColor({color: "rgb(231,226,155)"});
console.log("Starting extension", identifier);
finishSetup();

/* SETUP: options, context menu and event listeners */
async function finishSetup() {
	options = await browser.storage.local.get(defaultOptions);
	
	/* Create the context menu on the toolbar button */
	for (let d in settingsMenuItems) {
		browser.menus.create({
			contexts: ["browser_action"],
			checked: options[settingsMenuItems[d].option] === 
				(settingsMenuItems[d].value || true),
			onclick: handleButtonContextMenuClick,
			id: d,
			...settingsMenuItems[d].properties
		});
	}

	/* Create the context menu of the URLs in the toolbar-button-popup */
	let perms = (await browser.permissions.getAll()).permissions;
	contextMenuItems.uppity_copy.enabled = perms.includes("clipboardWrite");
	contextMenuItems.uppity_bookmark.enabled = perms.includes("bookmarks");
	for (let d in contextMenuItems) {
		if (!options.hideContextMenuItems.includes(d))
		browser.menus.create({
			documentUrlPatterns: [identifier+'popup.html'],
			contexts: ["page"],
			type: "normal",
			onclick: handlePopupContextMenuClick,
			id: d,
			...contextMenuItems[d]
		});
	}

	/* Set shortcut keys to off if that's the current setup */
	if (!options.shortcutKeys) {
		browser.commands.update({name: "upOneLevel", shortcut: ""});
		browser.commands.update({name: "_execute_browser_action", shortcut: ""});
	}
	
	/* LISTENERS: Update after a change in the URL: */
	/* 1. when a tab reports an 'update' to its URL */
	browser.tabs.onUpdated.addListener(handleTabUpdate, {properties: ['url']});
	/* 2. when a different tab becomes active */
	browser.tabs.onActivated.addListener(handleTabActivate);
	/* 3. when a different window becomes active */
	browser.windows.onFocusChanged.addListener(handleWindowFocus);

	/* LISTENERS: React to shortcut key Ctrl+Alt+Up (if the button is active) */
	browser.commands.onCommand.addListener(command => {
		if (command === "upOneLevel") up();
	});
	/* LISTENERS: React to changes in the optional permissions */
	browser.permissions.onAdded.addListener(perm => {
		if (perm.permissions.includes("clipboardWrite"))
			browser.menus.update("uppity_copy", {enabled: true});
		if (perm.permissions.includes("bookmarks"))
			browser.menus.update("uppity_bookmark", {enabled: true});
	});
	browser.permissions.onRemoved.addListener(perm => {
		if (perm.permissions.includes("clipboardWrite"))
			browser.menus.update("uppity_copy", {enabled: false});
		if (perm.permissions.includes("bookmarks"))
			browser.menus.update("uppity_bookmark", {enabled: false});
	});
	
	/* INITIALIZE the main variables and the toolbar button */
	if (options.buttonBehavior !== "popup")
		configureToolbarButton(options.buttonBehavior);
	let initial = await browser.tabs.query({active: true, currentWindow: true});
	if (initial.length) {
		currentWindowId = initial[0].windowId;
		currentTabId = initial[0].id;
		updateUponUrlChange(initial[0].url);
	}
}

/* HANDLERS */
function handleTabUpdate(tabId, details, tab) {
	/* Don't update the URL if this concerns another tab, if it's already
	 * correct, or if multiple updates including 'about:blank' follow
	 * quickly after each other (e.g. when opening a tab from cache) */
	if (tabId !== currentTabId || details.url === currentUrl ||
		(details.url === 'about:blank' && tab.lastAccessed && 
		tab.lastAccessed-lastUpdateTimestamp < DEBOUNCE_MS))
			return;
	if (tab.lastAccessed) lastUpdateTimestamp = tab.lastAccessed;
	updateUponUrlChange(details.url);
}

async function handleTabActivate(details) {
	/* Don't continue if this concerns another window or is already correct */
	if (details.windowId !== currentWindowId || details.tabId === currentTabId)
		return;
	currentTabId = details.tabId;
	let newTab = await browser.tabs.get(details.tabId);
	/* "loading" tab may not yet have the right URL, and only update if new */
	if (newTab.status !== "complete" || newTab.url === currentUrl)
		return;
	updateUponUrlChange(newTab.url);
}

async function handleWindowFocus(newId) {
	/* Don't continue if a different application got focus or we got back to
	 * where we just were before an app switch */
	if (newId === browser.windows.WINDOW_ID_NONE || newId === currentWindowId)
		return;
	let oldId = currentWindowId;  // we may need to restore this later
	currentWindowId = newId;  // in time for tabs.onActivated
	let newTabs = await browser.tabs.query({active: true, windowId: newId});
	/* Undo & stop if this is the Bookmarks window etc, which has no tabs */
	if (!newTabs.length) {
		currentWindowId = oldId;
		return;
	}
	/* 'Open in new window' first fires tabs.onActivated and then 
	 * windows.onFocusChanged, so handleTabActivate exits and nothing
	 * makes the change to currentTabId => it must be done here */
	currentTabId = newTabs[0].id;
	/* If multiple updates including 'about:blank' follow quickly after
	 * each other (e.g. when opening a tab from cache), stop evaluating */
	if (newTabs[0].url === 'about:blank' && newTabs[0].lastAccessed && 
		newTabs[0].lastAccessed-lastUpdateTimestamp < DEBOUNCE_MS)
			return;
	updateUponUrlChange(newTabs[0].url);
}

function handleButtonClick(tab, click) {
	if (!currentTargetUrls.next) return;
	if (click.button === 0)  // left-click
		browser.tabs.update(currentTabId, {'url': currentTargetUrls.next});
	else  // middle-click
		browser.tabs.create({'url': currentTargetUrls.next});
}

function handleButtonContextMenuClick(menudata) {
	if (menudata.checked === menudata.wasChecked) return;

	/* Update the options object and its saved values */
	let option = settingsMenuItems[menudata.menuItemId].option;
	let newValue = settingsMenuItems[menudata.menuItemId].value || menudata.checked;
	options[option] = newValue;
	browser.storage.local.set(options);	

	/* Make the changes that result from the new settings */
	switch (option) {
		case "shortcutKeys": {
			browser.commands.update({name: "upOneLevel",
				shortcut: newValue ? "Ctrl+Alt+Up" : ""});
			browser.commands.update({name: "_execute_browser_action",
				shortcut: newValue ? "Ctrl+Alt+Down" : ""});
			break;
		}
		case "buttonBehavior": {
			configureToolbarButton(newValue);
			break;
		}
		default:
			updateUponUrlChange(currentUrl); /* update list and badge */
	}
}

function handlePopupContextMenuClick(menudata) {
/* This level of indirection might seem unnecessary,
 * but this function gets fixed as-is to the 'onclick' key for the
 * context-menu items, so we can't use contextHandler there
 * directly as it couldn't be updated by popup.js
 */
	contextHandler(menudata);
}

/* UTILITY FUNCTIONS */
function up() {
	if (currentTargetUrls.next)
		browser.tabs.update(currentTabId, {'url': currentTargetUrls.next});
}

function getUrlsFor(url) {
	try {
		var urlParser = new URL(url);
	} catch(e) {
		console.error("getUrlsFor: error parsing", url);
		console.error(e);
		return {list: [], next: null};
	}

	/* 'about:', 'chrome:' and 'file:' URLs may not be opened by extensions;
	 * 'data:' and 'javascript:' have no meaningful directory hierarchy */
	if (options.disableForSchemes.includes(urlParser.protocol))
		return {list: [], next: null};

	let URLs = [],
		shortcutChoice = 0;

	function removeFromUrl(part) {
		if (urlParser[part]) {
			urlParser[part] = '';
			URLs.push(urlParser.href);
		}
	}
	function climbUpPath(part, endchar) {
		let pathparts = urlParser[part].split('/');
		if (pathparts.slice(-1)[0] === '')
			pathparts.pop();	// the part had a trailing slash
		if (pathparts[0] === '#')
			pathparts[0] = '';	// avoid leaving only '#/'
 		while (pathparts.length > 1) {
			pathparts.pop();
			urlParser[part] = pathparts.join('/') + endchar;
			URLs.push(urlParser.href);
		}
	}
	
	try {
		if (urlParser.hash && urlParser.hash.lastIndexOf('/') > 0)  // slash in the hash
			climbUpPath('hash', '');  // .hash = '#' + fragment
		removeFromUrl('hash');
		urlParser.hash = '';  // if url ended in '#' (so .hash='')

		if (urlParser.search && options.urlQueryParts) { // remove query one by one
			let params = Array.from(urlParser.searchParams.keys())  // ordered set
				.reduce((ar, el) => {if (!ar.includes(el)) ar.push(el); return ar;}, []);
			while (params.length) {
				urlParser.searchParams.delete(params.pop());
				URLs.push(urlParser.href);
			}		
		}
		removeFromUrl('search');  // .search = '?' + query
		urlParser.search = '';  // if url ended in '?' (so .search='')

		if (urlParser.pathname !== '/')
			climbUpPath('pathname', options.finalSlash ? '/' : '');
		removeFromUrl('password');  // username:password@  before the hostname
		removeFromUrl('username');
		removeFromUrl('port');  // the number after the domain name, between : and /

		if (!urlParser.hostname.match(/^[0-9.]{7,15}$/) /* IPv4 address */ &&
		  !urlParser.hostname.match(/^\[[0-9a-fA-F:]{2,39}\]$/) /* IPv6 address */ ) {
			let domainparts = urlParser.hostname.split('.');
			if (domainparts.length === 2 && options.addWwwToList) {
				/* only domain.tld means we put www.domain.tld at the end */
				urlParser.hostname = 'www.' + urlParser.hostname;
				URLs.push(urlParser.href);
			}
			while (domainparts.length > 2) {
				/* optionally insert www.* before abbreviating to two parts */
				if (domainparts.length === 3 && domainparts[0] !== 'www' &&
				  options.addWwwToList) {
					domainparts[0] = 'www';
					shortcutChoice = URLs.length ? 0 : 1;
					/* this ensures that if the entire list is just the inserted 
					 * www.domain.tld and domain.tld, the shortcut is for domain.tld */
				}
				else
					domainparts.shift();
				urlParser.hostname = domainparts.join('.');
				URLs.push(urlParser.href);
			}
		}
	} catch (e) {
		console.error("getUrlsFor: error parsing", url); console.error(e);
	}
	return {list: URLs,
			next: URLs.length ? URLs[shortcutChoice] : null}
}

function updateUponUrlChange(url) {
	currentUrl = url;

	/* If we're in reader mode, adjust the URL */
	if (url.slice(0, 17) === 'about:reader?url=')
		url = decodeURIComponent(url.slice(17));
	currentTargetUrls = getUrlsFor(url);
	
	/* update the toolbar icon */
	let l = currentTargetUrls.list.length;
	if (l) {
		browser.browserAction.enable();
		if (options.badgeOnButton)
			browser.browserAction.setBadgeText({text: l.toString()});
		else
			browser.browserAction.setBadgeText({text: ''});	
	} else {
		browser.browserAction.disable();
		browser.browserAction.setBadgeText({text: ''});
	}
}

function configureToolbarButton(behavior) {
	if (behavior === "popup") {
		/* Default behavior: only the popup */
		browser.browserAction.setPopup({popup: identifier+'popup.html'});
		browser.browserAction.onClicked.removeListener(up);
		browser.browserAction.onClicked.removeListener(handleButtonClick);
		return;
	}
	if (behavior === "up") {
		/* No popup. Left-click=up, middle-click=up in new tab */
		browser.browserAction.setPopup({popup: ''});
		browser.browserAction.onClicked.removeListener(up);
		browser.browserAction.onClicked.addListener(handleButtonClick);
		return;
	}
	if (behavior === "popup+upnewtab") {
		/* Left-click=show popup, middle-click=up in new tab */
		browser.browserAction.setPopup({popup: identifier+'popup.html'});
		browser.browserAction.onClicked.removeListener(up);
		browser.browserAction.onClicked.addListener(handleButtonClick);
		return;
	}
	if (behavior === "popup+up") {
		/* Left-click=show popup, middle-click=up in current tab */
		browser.browserAction.setPopup({popup: identifier+'popup.html'});
		browser.browserAction.onClicked.removeListener(handleButtonClick);
		browser.browserAction.onClicked.addListener(up);
		return;
	}
	console.warn(`Can't configure toolbar button "${behavior}" behavior`);
}
